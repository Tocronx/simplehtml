using System;
using System.Linq;
using Tocronx.SimpleHtml;
using Xunit;

namespace Tocronx.SimpleHtml.Tests
{
    public class HtmlElementTest
    {
        [Fact]
        public void SimpleTwoPartTagTest()
        {
            var html = "<body>Hello Wolrd!</body>";
            var htmlElement = new HtmlElement() { OuterHtml = html };
            Assert.Equal(html, htmlElement.OuterHtml);
            Assert.Equal("Hello Wolrd!", htmlElement.InnerHtml);
            Assert.Equal("Hello Wolrd!", htmlElement.Childs.First().InnerHtml);
        }

        [Fact]
        public void SimpleSingePartTagTest()
        {
            var html = "Hello<br />Wolrd!";
            var htmlElement = new HtmlElement() { TagName = "body", InnerHtml = html };
            Assert.Equal(html, htmlElement.InnerHtml);
        }

        [Fact]
        public void KomplexHtmlTest()
        {
            var html = "<body><b>Hello<br />Wolrd!</b><br />\n<br />\n<i>Sample text ...</i></body>";
            var htmlElement = new HtmlElement() { OuterHtml = html };
            Assert.Equal(html, htmlElement.OuterHtml);
        }


        [Fact]
        public void AttributeTest()
        {
            var html = "<body background='#FFF'><b>Hello<br />Wolrd!</b><br /><br /><i>Sample text ...</i></body>";
            var htmlElement = new HtmlElement() { OuterHtml = html };
            Assert.Equal("#FFF", htmlElement.Attributes.Where(x => x.Key == "background").Select(x => x.Value).Single());
        }

        [Theory]
        [InlineData("<body><body>")]
        [InlineData("</body><body>")]
        [InlineData("<body></body")]
        [InlineData("<body</body>")]
        [InlineData("<body1></body2>")]
        [InlineData("<body param></body>")]
        [InlineData("<body param=value></body")]
        [InlineData("<body><br></body>")]
        [InlineData("<body></test><test></body>")]
        [InlineData("<a><b><c></a></b></c>")]
        public void ParsingErrorTest(string html)
        {
            Assert.Throws<HtmlElement.HtmlParsingException>(() => new HtmlElement() { OuterHtml = html });
        }

        [Theory]
        [InlineData("<body></body>")]
        [InlineData("<body>Test<b>Test<br/>Test</b>Test</body>")]
        [InlineData("<body\nparam='test'></body>")]
        [InlineData("Hello<br/>World")]
        [InlineData("<a><b><c></c></b></a>")]
        [InlineData("<body\nparam='<tagparam>'></body>")]
        public void ParsingTest(string html)
        {
            var htmlElement = new HtmlElement() { OuterHtml = html };
            Assert.NotNull(htmlElement);
        }


    }
}
