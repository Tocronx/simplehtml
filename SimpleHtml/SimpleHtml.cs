﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tocronx.SimpleHtml
{
    public interface INodeElement
    {
        string InnerHtml { get; }
        string OuterHtml { get; }
    }

    public class TextElement : INodeElement
    {
        public string InnerHtml { get; set; }
        public string OuterHtml { get => InnerHtml; set => InnerHtml = value; }
    }

    public class HtmlElement : INodeElement
    {
        public class HtmlParsingException : ArgumentException
        {
            public HtmlParsingException(int charIndex) : base($"Invalid char at index {charIndex}") { }
        }

        public string TagName { get; set; }

        public string InnerHtml
        {
            get => Childs.Aggregate("", (a, b) => a + b.OuterHtml);
            set { Parse(value, true); }
        }

        public string OuterHtml
        {
            get
            {
                if (TagName == null)
                    return InnerHtml;
                else if (Childs.Count == 0)
                    return $"<{TagName}{Attributes.Aggregate(" ", (a, b) => a + $"{b.Key}=\"{b.Value}\"").TrimEnd()} />";
                else
                    return $"<{TagName}{Attributes.Aggregate(" ", (a, b) => a + $"{b.Key}=\"{b.Value}\"").TrimEnd()}>{InnerHtml}</{TagName}>";
            }
            set => Parse(value, false);
        }

        public List<KeyValuePair<string, string>> Attributes { get; } = new List<KeyValuePair<string, string>>();

        public List<INodeElement> Childs { get; } = new List<INodeElement>();

        private void Parse(string html, bool childsOnly)
        {
            Childs.Clear();
            if (!childsOnly) { TagName = null; Attributes.Clear(); }
            if (string.IsNullOrWhiteSpace(html))
            {
                if (html.Length > 0) { Childs.Add(new TextElement() { InnerHtml = html }); }
                return;
            }
            var elementStack = new Stack<HtmlElement>(new HtmlElement[] { this });
            var keyStack = new Stack<string>();
            int lastPos = 0;
            string text = null, key = null;
            bool isTag = false, isTagName = false, isAtt = false, isParam = false, isCloseTag = false;
            for (int i = 0; i < html.Length; i++)
            {
                var c = html[i];
                switch (c)
                {
                    case '!':
                        if (!isTagName) { break; }
                        if (lastPos == i && i + 2 < html.Length)
                        {
                            if (html[i + 1] == '-' && html[i + 2] == '-')
                            {
                                for (int z = i + 3; z < html.Length; z++)
                                {
                                    if (html[z] == '>' && html[z - 1] == '-' && html[z - 2] == '-')
                                    {
                                        lastPos = z + 1;
                                        break;
                                    }
                                }
                            }
                            if (i + 8 < html.Length && html.Substring(i + 1, 7).ToUpper() == "DOCTYPE")
                            {
                                for (int z = i + 8; z < html.Length; z++)
                                {
                                    if (html[z] == '>')
                                    {
                                        lastPos = z + 1;
                                        break;
                                    }
                                }
                            }
                        }
                        if (lastPos == i) { throw new HtmlParsingException(i); }
                        if (elementStack.Peek() != this)
                        {
                            elementStack.Pop();
                        }
                        i = lastPos - 1;
                        isTagName = false;
                        isTag = false;
                        break;
                    case '=':
                        if (isParam || !isTag) { break; }
                        if (isTagName) { throw new HtmlParsingException(i); }
                        key = Extract(i, false);
                        if (key == null) { throw new HtmlParsingException(i); }
                        keyStack.Push(key);
                        isAtt = true;
                        break;
                    case '\'':
                    case '"':
                        if (!isTag) { break; }
                        var value = Extract(i, false);
                        if (isParam)
                        {
                            key = keyStack.Pop();
                            elementStack.Peek().Attributes.Add(new KeyValuePair<string, string>(key, value ?? ""));
                            isAtt = false;
                        }
                        else
                        {
                            if (!isAtt || !string.IsNullOrWhiteSpace(value)) { throw new HtmlParsingException(i); }
                        }
                        isParam = !isParam;
                        break;
                    case '<':
                        if (isParam) { break; }
                        if (isTag) { throw new HtmlParsingException(i); }
                        text = Extract(i, false);
                        if (!string.IsNullOrEmpty(text))
                        {
                            elementStack.Peek().Childs.Add(new TextElement() { InnerHtml = text });
                        }
                        if (childsOnly || i > 0)
                        {
                            var el = new HtmlElement();
                            elementStack.Peek().Childs.Add(el);
                            elementStack.Push(el);
                        }
                        else if (Childs.Count > 0) { throw new HtmlParsingException(i); }
                        isTag = true;
                        isTagName = true;
                        break;
                    case '>':
                        if (isParam) { break; }
                        if (!isTag || isAtt) { throw new HtmlParsingException(i); }
                        CheckTagName(Extract(i, false), i);
                        if (isCloseTag)
                        {
                            if (elementStack.Count < 2) { throw new HtmlParsingException(i); }
                            var element = elementStack.Pop();
                            elementStack.Peek().Childs.Remove(element);
                            if (element.TagName != elementStack.Peek().TagName) { throw new HtmlParsingException(i); }
                            elementStack.Pop();
                            isCloseTag = false;
                        }
                        isTag = false;
                        break;
                    case '/':
                        if (isParam || !isTag) { break; }
                        if (i > 0 && html[i - 1] == '<')
                        {
                            isCloseTag = true;
                            lastPos = i + 1;
                            break;
                        }
                        if (!isAtt && i < html.Length && html[i + 1] == '>')
                        {
                            CheckTagName(Extract(i, false), i);
                            if (elementStack.Count < 2) { throw new HtmlParsingException(i); }
                            elementStack.Pop();
                            break;
                        }
                        throw new HtmlParsingException(i);
                    case ' ':
                    case '\n':
                    case '\r':
                        if (isTagName) { CheckTagName(Extract(i, false), i); }
                        break;
                }
            }
            if (isTag || elementStack.Count > 1) { throw new HtmlParsingException(html.Length); }
            text = Extract(html.Length - 1, true);
            if (!string.IsNullOrWhiteSpace(text)) { elementStack.Peek().Childs.Add(new TextElement() { InnerHtml = text }); }
            void CheckTagName(string t, int pos)
            {
                bool isEmpty = string.IsNullOrWhiteSpace(t);
                if (isEmpty == isTagName) { throw new HtmlParsingException(pos); }
                if (!isEmpty) { elementStack.Peek().TagName = t.Trim(); }
                isTagName = false;
            }
            string Extract(int pos, bool incl)
            {
                var lPos = lastPos;
                lastPos = pos + 1;
                var len = pos - lPos + (incl ? 1 : 0);
                return (len > 0) ? html.Substring(lPos, len) : null;
            }
        }

        public IEnumerable<INodeElement> VisitChildsRecursive()
        {
            foreach (var child in Childs)
            {
                yield return child;
                if (child is HtmlElement htmlElement)
                {
                    foreach (var item in htmlElement.VisitChildsRecursive())
                    {
                        yield return item;
                    }
                }
            }
        }

    }
}
