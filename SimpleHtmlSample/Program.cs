﻿using System;
using System.Text;
using Tocronx.SimpleHtml;

namespace Tocronx.SimpleHtmlSample.Sample
{
    static class Program
    {
        static void Main(string[] args)
        {
            var srcHtml = "<!DOCTYPE html>" +
                        "<html>" +
                        "<head>" +
                        "<meta charset='UTF-8'/>" +
                        "<title>Title of the document</title>" +
                        "</head>" +
                        "<body>" +
                        "<h1>Content of the document......</h1><br />" +
                        "<div background='#FFF'><b>Hello<br />Wolrd!</b><br /><br /><i>Sample text ...</i></div>" +
                        "</body>" +
                        "</html>";

            var htmlElement = new HtmlElement();
            htmlElement.OuterHtml = srcHtml;

            var resultHtml = htmlElement.OuterHtml;
            Console.WriteLine(resultHtml);

            Console.WriteLine("");

            foreach (var node in htmlElement.VisitChildsRecursive())
            {
                if (node is TextElement te)
                {
                    Console.WriteLine("Text: " + te.InnerHtml);
                }
                if (node is HtmlElement hr)
                {
                    Console.WriteLine("Tag:" + hr.TagName);
                }
            }

            Console.ReadKey();
        }
    }
}
